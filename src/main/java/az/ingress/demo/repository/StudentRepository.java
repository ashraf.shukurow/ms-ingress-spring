package az.ingress.demo.repository;

import az.ingress.demo.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author ashraf on 27.01.24
 * @demo
 */
public interface StudentRepository extends JpaRepository<Student,Long> {
    Optional<Student> findByFin(String fin);
    boolean existsByFin(String fin);
}
