package az.ingress.demo.mapper;

import az.ingress.demo.model.dto.request.StudentRequest;
import az.ingress.demo.model.dto.response.StudentResponse;
import az.ingress.demo.model.entity.Student;
import org.mapstruct.Mapper;

/**
 * @author ashraf on 31.01.24
 * @demo
 */

@Mapper(componentModel = "spring")
public interface StudentMapper {
    Student requestToEntity(StudentRequest studentRequest);

    StudentResponse entityToResponse(Student student);

}
