package az.ingress.demo.service.impl;

import az.ingress.demo.exception.AlreadyExistsException;
import az.ingress.demo.exception.NotFoundException;
import az.ingress.demo.mapper.StudentMapper;
import az.ingress.demo.model.dto.request.StudentRequest;
import az.ingress.demo.model.dto.response.StudentResponse;
import az.ingress.demo.model.entity.Student;
import az.ingress.demo.repository.StudentRepository;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ashraf on 23.01.24
 * @demo
 */
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;


    @Override
    public void addStudent(StudentRequest studentRequest) {
        Student student=studentMapper.requestToEntity(studentRequest);
        if(studentRepository.existsByFin(student.getFin())){
            throw new AlreadyExistsException("There is Student with such  fin="+student.getFin());
        }
       else{
            studentRepository.save(student);
        }
    }

    @Override
    public List<StudentResponse> getAllStudent() {
        List<Student> students=studentRepository.findAll();
        return students.stream().map(studentMapper::entityToResponse).toList();

    }

    @Override
    public StudentResponse getStudentById(Long id) {
        Student student=studentRepository.findById(id).orElseThrow(()->new NotFoundException("there is no student with this id:"+id));
        return studentMapper.entityToResponse(student);
    }

    @Override
    public void deleteStudent(Long id) {
        Student student=studentRepository.findById(id).orElseThrow(()->new NotFoundException("there is no student with this id:"+id));
        studentRepository.delete(student);
    }


    @Override
    public void updateStudent(StudentRequest studentRequest) {
         Student student=studentRepository.findByFin(studentRequest.getFin()).orElseThrow(()->new NotFoundException("Student not found with this fin:"+studentRequest.getFin()));
         Student updatedStudent=studentMapper.requestToEntity(studentRequest);
         updatedStudent.setId(student.getId());
         updatedStudent.setFin(student.getFin());
         studentRepository.save(updatedStudent);

    }
}
