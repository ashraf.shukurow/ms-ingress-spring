package az.ingress.demo.service;

import az.ingress.demo.model.dto.request.StudentRequest;
import az.ingress.demo.model.dto.response.StudentResponse;
import az.ingress.demo.model.entity.Student;

import java.util.List;

/**
 * @author ashraf on 27.01.24
 * @demo
 */
public interface StudentService {
    void addStudent(StudentRequest studentRequest);
    List<StudentResponse> getAllStudent();
    StudentResponse getStudentById(Long id);
    void deleteStudent(Long id);
    void updateStudent(StudentRequest studentRequest);


}
