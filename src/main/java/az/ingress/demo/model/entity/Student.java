package az.ingress.demo.model.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author ashraf on 27.01.24
 * @demo
 */
@Entity
@Table(name = "student")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    int age;
    String surname;
    String email;
    String address;
    String fin;
}
