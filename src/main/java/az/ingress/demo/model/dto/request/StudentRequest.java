package az.ingress.demo.model.dto.request;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author ashraf on 19.01.24
 * @demo
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class StudentRequest {

     String name;
     String surname;
     int age;
     String address;
     String fin;
     String email;



}
