package az.ingress.demo.model.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author ashraf on 31.01.24
 * @demo
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentResponse {
    Long id;
    String name;
    String surname;
    int age;
    String address;
    String fin;
    String email;
}
