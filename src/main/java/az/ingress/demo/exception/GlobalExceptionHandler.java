package az.ingress.demo.exception;

import az.ingress.demo.model.dto.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

/**
 * @author ashraf on 31.01.24
 * @demo
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {
            AlreadyExistsException.class,
            NotFoundException.class
    })
    public ResponseEntity<ErrorResponse> handleException(Exception ex){
        ErrorResponse errorResponse=new ErrorResponse();
        errorResponse.setErrorMessage(ex.getMessage());
        errorResponse.setErrorCode(errorResponse.getErrorCode());
        errorResponse.setDate(LocalDateTime.now());
        errorResponse.setStatus(getHttpStatus(ex));
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);

    }
    private HttpStatus getHttpStatus(Exception ex){
        if(ex instanceof NotFoundException){
            return HttpStatus.NOT_FOUND;
        }else if(ex instanceof AlreadyExistsException){
            return HttpStatus.CONFLICT;
        }else{
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

}
