package az.ingress.demo.controller;

import az.ingress.demo.model.dto.request.StudentRequest;
import az.ingress.demo.model.dto.response.StudentResponse;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ashraf on 19.01.24
 * @demo
 */
@RestController
@RequestMapping("v1/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService service;

    @PostMapping("/")
    public void createStudent(@RequestBody StudentRequest studentRequest){
        service.addStudent(studentRequest);
    }
    @GetMapping("/{id}")
    public ResponseEntity<StudentResponse> getStudentById(@PathVariable Long id){
        return ResponseEntity.ok(service.getStudentById(id));
    }

    @GetMapping("/")
    public ResponseEntity<List<StudentResponse>> getAllStudent(){
        return ResponseEntity.ok(service.getAllStudent());
    }
    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable Long id){
        service.deleteStudent(id);
    }
    @PutMapping("/")
    public void updateStudent(@RequestBody StudentRequest studentRequest){
        service.updateStudent(studentRequest);
    }


}
